#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdint.h>
#include "instab.h"

void disasm(FILE *, int, FILE *);
int printmode(FILE *, int, int *, FILE *);
void printhelp(char *);
void version(char *);

int main(int argc, char *argv[]) {
  char opts[]="a:hi:o:v";
  int i, o;
  int addr=0;
  char *infile, *outfile;
  FILE *in, *out;

  infile=outfile=NULL;
  while((o=getopt(argc, argv, opts))!=-1) {
    switch(o) {
    case 'a':
      addr=0;
      if (optarg[0]=='0') {
	if (optarg[1]=='x' || optarg[1]=='X') {
	  for(i=2; isxdigit(optarg[i]); i++) {
	    if (isdigit(optarg[i])) addr=(addr<<4)|(optarg[i]-'0');
	    else addr=(addr<<4)|((optarg[i]&0x5f)+10-'A');
	  }
	} else {
	  for(i=1; optarg[i]>='0' && optarg[i]<='7'; i++)
	    addr=(addr<<3)|(optarg[i]-'0');
	}
      } else if (isdigit(optarg[0])) {
	for(i=0; isdigit(optarg[i]); i++)
	  addr=addr*10+optarg[i]-'0';
      } else {
	fprintf(stderr, "%s: Not a number\n", optarg);
	return -1;
      }
      if (addr<0 || addr>0xffff) {
	fprintf(stderr, "%s: Out of range\n", optarg);
	return -1;
      }
      break;
    case 'h':
      printhelp(argv[0]);
      return 0;
    case 'i':
      if (infile) {
	fprintf(stderr, "Multiple input 1 files specified\n");
	return -1;
      }
      infile=optarg;
      break;
    case 'o':
      if (outfile) {
	fprintf(stderr, "Multiple output files specified\n");
	return -1;
      }
      outfile=optarg;
      break;
    case 'v':
      version(argv[0]);
      break;
    default:
      fprintf(stderr, "Unk opt\n");
      printhelp(argv[0]);
      return -1;
    }
  }
  if (outfile) {
    if (!(out=fopen(outfile, "w"))) {
      fprintf(stderr, "Unable to write output file: %s\n", outfile);
      return -1;
    }
  } else out=stdout;
  if (optind<argc-2 || optind==argc-1 && infile) {
    fprintf(stderr, "Multiple input 2 files specified\n");
    return -1;
  }
  if (!infile && optind != argc) infile=argv[optind];
  if (infile) {
    if (!(in=fopen(infile, "r"))) {
      fprintf(stderr, "Unable to read input file: %s\n", infile);
      return -1;
    }
  } else in=stdin;
  disasm(in, addr, out);
  return 0;
}

void disasm(FILE *in, int addr, FILE *out) {
  int i, c;
  fprintf(out, "     org $%04x\n", addr);
  for(i=addr; (c=getc(in))!=EOF;) {
    if (c==0x10) {
      if ((c=getc(in))==EOF) break;
      if (ins10[c].mnem) {
	fprintf(out, "%04x %s ", i, ins10[c].mnem);
	i+=2;
	printmode(in, ins10[c].mode, &i, out);
      } else {
	fprintf(out, "%04x $10%02x\n", i, c);
	i+=2;
      }
    } else if (c==0x11) {
      if ((c=getc(in))==EOF) break;
      if (ins11[c].mnem) {
	fprintf(out, "%04x %s ", i, ins11[c].mnem);
	i+=2;
	printmode(in, ins11[c].mode, &i, out);
      } else {
	fprintf(out, "%04x $11%02x\n", i, c);
	i+=2;
      }
    } else {
      if (ins[c].mnem) {
	if (c==0x34 || c==0x35) {
	  pp[6]="u";
	} else if (c==0x36 || c==0x37) {
	  pp[6]="s";
	}
	fprintf(out, "%04x %s ", i, ins[c].mnem);
	i++;
	printmode(in, ins[c].mode, &i, out);
      } else {
	fprintf(out, "%04x $%02x\n", i, c);
	i++;
      }
    }
  }
}

int printmode(FILE *in, int mode, int *addr, FILE *out) {
  uint8_t u8, reg, ixmd;
  int8_t s8;
  uint16_t u16;
  int16_t s16;
  uint32_t u32;
  int i;
  
  switch(mode) {
  case DIR:
    u8=getc(in);
    (*addr)++;
    fprintf(out, "<$%02x\n", u8);
    break;
  case IPSH:
    u8=getc(in);
    (*addr)++;
    for(i=7; u8; i--) {
      if (u8&0x80) {
	if (u8!=0x80) fprintf(out, "%s,", pp[i]);
	else fprintf(out, "%s\n", pp[i]);
      }
      u8 <<= 1;
    }
    break;
  case IPUL:
    u8=getc(in);
    (*addr)++;
    for(i=0; u8; i++) {
      if (u8&1) {
	if (u8!=1) fprintf(out, "%s,", pp[i]);
	else fprintf(out, "%s\n", pp[i]);
      }
      u8 >>= 1;
    }
    break;
  case REL1:
    s8=getc(in);
    (*addr)++;
    u16=*addr+s8;
    fprintf(out, "$%04x\n", u16);
    break;
  case REL2:
    s16=getc(in);
    s16=(s16<<8)+getc(in);
    (*addr)+=2;
    u16=*addr+s16;
    fprintf(out, "$%04x\n", u16);
    break;
  case IXED:
    u8=getc(in);
    (*addr)++;
    reg=(u8>>6)&0x03;
    switch(reg) {
    case 0: reg='x'; break;
    case 1: reg='y'; break;
    case 2: reg='u'; break;
    case 3: reg='s'; break;
    }
    ixmd=u8&0x9f;
    if (ixmd<0x10) {
      fprintf(out, "$%x,%c\n", ixmd, reg);
    } else if (ixmd<32) {
      fprintf(out, "-$%x,%c\n", 32-ixmd, reg);
    } else {
      switch(ixmd) {
      case 0x80: fprintf(out, ",%c+\n", reg); break;
      case 0x81: fprintf(out, ",%c++\n", reg); break;
      case 0x82: fprintf(out, ",-%c\n", reg); break;
      case 0x83: fprintf(out, ",--%c\n", reg); break;
      case 0x84: fprintf(out, ",%c\n", reg); break;
      case 0x85: fprintf(out, "b,%c\n", reg); break;
      case 0x86: fprintf(out, "a,%c\n", reg); break;
      case 0x87: fprintf(out, "e,%c\n", reg); break;
      case 0x88:
	u8=getc(in);
	(*addr)++;
	if (u8<0x80) fprintf(out, "$%02x,%c", u8, reg);
	else fprintf(out, "-$%02x,%c", 0x100-u8, reg);
	if (u8<0x10 || u8 >=0xf0) fputs("\t8-bit\n", out);
	else putc('\n', out);
	break;
      case 0x89:
	u16=getc(in);
	u16=(u16<<8)|getc(in);
	(*addr)+=2;
	if (u16<0x8000) fprintf(out, "$%04x,%c\n", u16, reg);
	else fprintf(out, "-$%04x,%c\n", 0x10000-u16, reg);
	break;
      case 0x8a: fprintf(out, "f,%c\n", reg); break;
      case 0x8b: fprintf(out, "d,%c\n", reg); break;
      case 0x8c:
	u8=getc(in);
	(*addr)++;
	if (u8<0x80) u16=(*addr)+u8;
	else u16=(*addr)+u8-0x100;
	fprintf(out, "$%04x,pc\n", u16);
	break;
      case 0x8d:
	u16=getc(in);
	u16=(u16<<8)|getc(in);
	(*addr)+=2;
	u16+=*addr;
	fprintf(out, "$%04x,pc\n", u16);
	break;
      case 0x8e: fprintf(out, "w,%c\n", reg); break;
      case 0x8f:
	if (reg=='x') fprintf(out, ",w\n");
	else if (reg=='y') {
	  u16=getc(in);
	  u16=(u16<<8)|getc(in);
	  (*addr)+=2;
	  if (u16<0x8000) fprintf(out, "$%04x,w\n", u16);
	  else fprintf(out, "-$%04x,w\n", 0x10000-u16);
	} else if (reg=='u') fprintf(out, ",w++\n");
	else fprintf(out, ",--w\n");
	break;
      case 0x90:
	if (reg=='x') fprintf(out, "[,w]\n");
	else if (reg=='y') {
	  u16=getc(in);
	  u16=(u16<<8)|getc(in);
	  (*addr)+=2;
	  if (u16<0x8000) fprintf(out, "[$%04x,w]\n", u16);
	  else fprintf(out, "[-$%04x,w]\n", 0x10000-u16);
	} else if (reg=='u') fprintf(out, "[,w++]\n");
	else fprintf(out, "[,--w]\n");
	break;
      case 0x91: fprintf(out, "[,%c++]\n", reg); break;
      case 0x93: fprintf(out, "[,--%c]\n", reg); break;
      case 0x94: fprintf(out, "[,%c]\n", reg); break;
      case 0x95: fprintf(out, "[b,%c]\n", reg); break;
      case 0x96: fprintf(out, "[a,%c]\n", reg); break;
      case 0x97: fprintf(out, "[e,%c]\n", reg); break;
      case 0x98:
	u8=getc(in);
	(*addr)++;
	if (u8<0x80) fprintf(out, "[$%02x,%c]\n", u8, reg);
	else fprintf(out, "[-$%02x,%c]\n", 0x100-u8, reg);
	break;
      case 0x99:
	u16=getc(in);
	u16=(u16<<8)|getc(in);
	(*addr)+=2;
	if (u16<0x8000) fprintf(out, "[$%04x,%c]\n", u16, reg);
	else fprintf(out, "[-$%04x,%c]\n", 0x10000-u16, reg);
	break;
      case 0x9a: fprintf(out, "[f,%c]\n", reg); break;
      case 0x9b: fprintf(out, "[d,%c]\n", reg); break;
      case 0x9c:
	u8=getc(in);
	(*addr)++;
	if (u8<0x80) u16=(*addr)+u8;
	else u16=(*addr)+u8-0x100;
	fprintf(out, "[$%04x,pc]\n", u16);
	break;
      case 0x9d:
	u16=getc(in);
	u16=(u16<<8)|getc(in);
	(*addr)+=2;
	u16+=*addr;
	fprintf(out, "[$%04x,pc]\n", u16);
	break;
      case 0x9e: fprintf(out, "[w,%c]\n", reg); break;
      case 0x9f:
	u16=getc(in);
	u16=(u16<<8)|getc(in);
	(*addr)+=2;
	fprintf(out, "[$%04x]\n", u16);
	break;
      default:
	fprintf(out, "x%02x\n", u8);
      }
    }
    break;
  case EXT:
    u16=getc(in);
    u16=(u16<<8)+getc(in);
    (*addr)+=2;
    fprintf(out, ">$%04x\n", u16);
    break;
  case IMM1:
    u8=getc(in);
    (*addr)++;
    fprintf(out, "#$%02x\n", u8);
    break;
  case IMM2:
    u16=getc(in);
    u16=(u16<<8)+getc(in);
    (*addr)+=2;
    fprintf(out, "#$%04x\n", u16);
    break;
  case IMM4:
    u32=getc(in);
    u32=(u32<<8)+getc(in);
    u32=(u32<<8)+getc(in);
    u32=(u32<<8)+getc(in);
    (*addr)+=4;
    fprintf(out, "#$%08x\n", u32);
    break;
  case REG:
    u8=getc(in);
    (*addr)++;
    fprintf(out, "%s,%s\n", regs[u8>>4], regs[u8&0xf]);
    break;
  case NONE:
  case INH0:
  default:
    putc('\n', out);
    break;
  }
  return 0;
}
      
void printhelp(char *command) {
  version(command);
  putchar('\n');
  printf("%s [<options>] [<infile>]\n", command);
  printf("options:\n");
  printf("\t-a <address>\tThe start address\n");
  printf("\t-h\t\tThis help message\n");
  printf("\t-i <input>\tThe input file\n");
  printf("\t-o <output>\tThe output file\n");
  printf("\t-v\t\tPrint version info\n");
  printf("\nUnspecified files default to stdin and stdout\n");
}

void version(char *command) {
  printf("%s version 1.00.00 12 Nov 2020\n", command);
  printf("By Theodore (Alex) Evans 2020\n");
}
