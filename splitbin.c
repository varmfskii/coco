#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

typedef struct program {
  char *buf;
  u_int size;
} program;
  
void splitbin(char *);

static inline uint8_t read8(uint8_t *s) {
  return *s;
}

static inline uint16_t read16(uint8_t *s) {
  return (s[0]<<8)|(s[1]);
}
    
int main(int argc, char *argv[]) {
  int i;
  for(i=1; i<argc; i++) splitbin(argv[i]);
  return 0;
}

void splitbin(char *filename) {
  FILE *in;
  struct stat s;
  program bin;
  char *outname;
  uint8_t blk_type;
  uint16_t addr, len;
  int i;
  FILE *out;
  
  if(stat(filename, &s)) {
    perror(filename);
    return;
  }
  outname=malloc(strlen(filename+5));
  bin.size=s.st_size;
  if(!(bin.buf=malloc(bin.size))) {
    fprintf(stderr, "Allocation error\n");
    return;
  }
  if (!(in=fopen(filename, "r"))) {
    fprintf(stderr, "Cannot open: %s\n", filename);
    free(bin.buf);
    return;
  }
  fread(bin.buf, 1, bin.size, in);
  fclose(in);
  for(i=0; i<bin.size;) {
    blk_type=read8(bin.buf+i); i++;
    if (blk_type==0) {
      len=read16(bin.buf+i); i+=2;
      addr=read16(bin.buf+i); i+=2;
      sprintf(outname, "%s%04x", filename, addr);
      out=fopen(outname, "w");
      fwrite(bin.buf+i, 1, len, out);
      fclose(out);
      i+=len;
    } else if (blk_type==0xff) {
      len=read16(bin.buf+i); i+=2;
      addr=read16(bin.buf+i); i+=2;
      printf("Execution address: %04x\n", addr);
    } else {
      fprintf(stderr, "Unknown block: %02x\n", blk_type);
      return;
    }
  }
  free(bin.buf);
  return;
}
