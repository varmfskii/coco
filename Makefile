CFLAGS += -g
TGTS=splitbin dis09
SRCS=dis09.c instab.c splitbin.c
OBJS=$(SRCS:.c=.o)
DEPS=$(SRCS:.c=.d)

all: $(TGTS)

splitbin: splitbin.o

dis09: dis09.o instab.o

%.d: %.c
	gcc -MM -MF $@ $<

include $(DEPS)

.PHONY: all clean distclean

clean:
	rm -f $(OBJS) $(DEPS) *~

distclean: clean
	rm -f $(TGTS)
