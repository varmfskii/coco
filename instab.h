#define NONE 0
#define DIR  1
#define INH0 2
#define IPSH 3
#define IPUL 4
#define REL1 5
#define REL2 6
#define IXED 7
#define EXT  8
#define IMM1 9
#define IMM2 10
#define IMM4 11
#define REG 12

typedef struct inst {
  char *mnem;
  int mode;
} inst;

extern inst ins[256], ins10[256], ins11[256];
extern char *regs[16], *pp[8];
